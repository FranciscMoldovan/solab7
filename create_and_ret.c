#define _OPEN_THREADS
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void *threadFunc(void *arg) 
{
  char *ret;
  printf("\t[HELLO from thread] entered with argument '%s'\n", (char*)arg);
  if ((ret = (char*) malloc(20)) == NULL) 
  {
    perror("malloc() error");
    exit(2);
  }
  
  strcpy(ret, "This is a test");
  pthread_exit(ret);
}

int main() {
  pthread_t thid;
  void *ret;

  if (pthread_create(&thid, NULL, threadFunc, "thread 1") != 0) 
  {
    perror("pthread_create() error");
    exit(1);
  }

  if (pthread_join(thid, &ret) != 0) 
  {
    perror("pthread_create() error");
    exit(3);
  }

  printf("[Thread MAIN] thread exited with '%s'\n", (char*)ret);

return 0;
}