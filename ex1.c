#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *thFunc(void*arg)
{
  int *val = (int*)arg;
  for(;;)
  {
    printf("Th with argument %d\n", *val);
    sleep(1);
  }
}

int main()
{
  pthread_t th1;
  int arg1 = 1;
  
  pthread_create(&th1, NULL, thFunc, &arg1);
  pthread_join(th1, NULL);

return 0;
}
