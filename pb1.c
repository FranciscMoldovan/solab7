#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <unistd.h>

#include <pthread.h>

#define MAXSTEPS 4
#define NAMELEN 25

void* execThread(void *threadName)
{
    for(;;)
    {
        printf("[thread:name=%s]\n", (char*)threadName);
        sleep(1);
    }
}

int main(int argc, char **argv)
{
    int idx = 0;
    int numThreads = 0;
    pthread_t *threads;
    char **threadNames;
    int *state;
 
    if(2!=argc)
    {
        printf("USAGE: %s NUM_THREADS\n", argv[0]);
        exit(-1);
    }

    sscanf(argv[1], "%d", &numThreads);

    printf("[MAIN Thread] Creating %d threads\n", numThreads);
    threads = (pthread_t*)malloc(numThreads*sizeof(pthread_t));
    threadNames = (char**)malloc(numThreads*sizeof(char*));
    state = (int*)malloc(numThreads*sizeof(int));

    for(idx = 0; idx <numThreads; idx++)
    {
        threadNames[idx] = (char*)malloc(NAMELEN*sizeof(char));
        sprintf(threadNames[idx], "%d", idx);

        if(pthread_create(&threads[idx], NULL, execThread, threadNames[idx])!=0)
        {
            perror("ERROR create thread!\n");
        }

    }

    for(idx = 0; idx <numThreads; idx++)
    {
        pthread_join(threads[idx], (void**)&state[idx]);
    }


    for(idx=0; idx<numThreads; idx++)
    {
        free(threadNames[idx]);
    }
    free(threadNames);
    free(threads);
    free(state);
    
    return 0;
}

