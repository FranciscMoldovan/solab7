/*
Write a program that specifies the maximum number of threads that 
can simultaneously exist in the same process. To avoid overloading 
the processor, the created threads will call the function sleep inside 
an infinite loop. To find the wanted number, the value returned by 
the pthread_create function is tested, so to detect the moment when 
no more threads can be created 
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <unistd.h>

#include <pthread.h>

void *thFunc(void *thFuncArgs)
{
    printf("[Hello from th:%ld]", pthread_self());
    for(;;)
    {
        sleep(1);
    }

    return 0;
}

int main(int argc, char **argv)
{
    int maxThreadCount=-1;
    int threadCount=0;
    int i;

    pthread_t* threads;

    if(argc != 2)
    {
        printf("USAGE: %s MAX_TH_CNT\n", argv[0]);
        exit(-1);
    }

    sscanf(argv[1], "%d", &maxThreadCount);

    if(maxThreadCount == -1)
    {
        printf("[Main th, ERROR], BAD ARGUMENT\n");
        exit(-2);
    }

    threads = (pthread_t*)malloc(maxThreadCount*sizeof(pthread_t));

    while(threadCount <= maxThreadCount)
    {
        if (pthread_create(&threads[threadCount], NULL, thFunc, NULL)!=-1)
        {
            printf("[Thread MAIN] Created a Thread!\n");
            threadCount++;
        }
    }

    for(i=0; i<maxThreadCount; i++)
    {
        pthread_join(threads[i], NULL);
    }

return 0;
}










