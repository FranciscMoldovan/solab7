/*
Write a server type C program that periodically creates threads that 
simulate   the   handling   of   some   requests   from   clients.   The   server 
threads display a message, wait for a while (using  sleep  function) 
and   then   terminate.  In   the   same   time,  the   server  also   accepts 
commands introduced from keyboard. Implement the stop command 
of the server,  like when  a special key is pressed (for example ‘x’): 
the process should  not create new threads and end. But ending the 
process should be done in a clean state, only after the termination of 
the existing threads at that time.  
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <unistd.h>

#include <pthread.h>

void *func_killer(void *args)
{
    pthread_t *thPtr = (pthread_t*)args;
    for(;;)
    {
        char x;
        scanf("%c", &x);
        if(x=='x')
        {
            pthread_cancel(*thPtr);
        }
    }
}

void *client_func(void *args)
{
    printf("[CLIENT Thread]\n");
    for(;;)
        sleep(5);
}

void *server_func(void *args)
{
    for(;;)
    {
        printf("~~~~SERVER: creating a client\n");
        pthread_t aThread;
        pthread_create(&aThread, NULL, client_func, NULL);
        sleep(3);
    }
}

int main(int argc, char **argv)
{
    printf("PID=%d\n", getpid());

    pthread_t th_server, th_killer;
    pthread_create(&th_server, NULL, server_func, NULL);
    pthread_create(&th_killer, NULL, func_killer, &th_server);

    pthread_join(th_killer, NULL);
    return 0;
}






