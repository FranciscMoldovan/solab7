//LABUL TRECUT, CU PROCESE!!!

#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int main(void) {
    int fd;
    int depth = 0; /* keep track of number of generations from original */
    int i;
    int childStatus;

    fd = open("fis.txt", O_WRONLY);
  
    for(i=0; i<3; i++) {
        if(fork() == 0) {  /* fork returns 0 in the child */
            int sts = write(fd, &i, 1);  /* write one byte into the pipe */      
            printf("my PID was:%d, [sts=%d], I was in depth %d \n", getpid(), sts, i);
            depth += 1;
        } else 
        {
            wait(&childStatus);
        }
    }

    close(fd);  

    if( depth == 0 ) { /* original process */
      i=0;

      fd = open("fis.txt", O_RDONLY);
      while(read(fd,&depth,1) != 0)
      {
        i += 1;
      }
      printf( "%d total processes spawned\n", i);
    }
    return 0;
}
