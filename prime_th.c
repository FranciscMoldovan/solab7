/* sa se numare cate numere prime sunt in intervalul a, b
pe n thread-uri, a, b, n sunt command line arguments.
Se va face implementarea naiva, in care se imparte intervalul in n parti
si se face un thread pt fiecare apoi main insumeaza ce au adunat
celelalte */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <unistd.h>

#include <pthread.h>

typedef struct data{
    int interStart;
    int interEnd;
    int numThreads;
    int isError;
}DataT;

typedef struct subinterval{
    int start;
    int end;
    int counted;
}SubintervalT;

int is_prime(int num)
{
     if (num <= 1) return 0;
     if (num % 2 == 0 && num > 2) return 0;
     for(int i = 3; i < num / 2; i+= 2)
     {
         if (num % i == 0)
             return 0;
     }
     return 1;
}

void *countPrimes(void *arguments)
{
    int *start, *end, *returned;

    start    = (int*)arguments;
    end      = (int*)arguments+1;
    returned = (int*)arguments+2;

    int i, count=0;
    for(i=*start; i<*end; i++)
    {
        if(is_prime(i))
        {
            count++;
        }
    }

   *returned = count;

    printf("\t [THREAD%ld]Hello[%d->%d]Found %d primes!\n", (long int)pthread_self(),\
        *start, *end, *returned);

    pthread_exit(returned);
}

DataT parseArgs(int argc, char**argv)
{
    DataT retData = {-1, -1, -1, 0};

    if (argc != 4)
    {
        printf("USAGE: %s INTERVAL_START INTERVAL_END NUM_THREADS\n", argv[0]);
        exit(-1);
    } else 
    {
        sscanf(argv[1], "%d", &retData.interStart);
        sscanf(argv[2], "%d", &retData.interEnd);
        sscanf(argv[3], "%d", &retData.numThreads);

        if (retData.numThreads < 0)
        {
            printf("[parsing ERROR] number of threads not OK\n");
            retData.isError = 1;
        }

        if (retData.interStart < 0 || retData.interEnd < 0)
        {
            printf("[parsing ERROR] You didn't input numers!\n");
            retData.isError = 1;
        }

        if (!(retData.interStart < retData.interEnd))
        {
            printf("[parsing ERROR] INTERVAL NOT OK!\n");
            retData.isError = 1;
        }

        if (retData.interEnd - retData.interStart < retData.numThreads)
        {
            printf("[parsing ERROR] TOO MANY THREADS!\n");
            retData.isError = 1;
        }
    }

  
    return retData;
    
}

int main(int argc, char **argv)
{
    DataT data = parseArgs(argc, argv);
    int i;
    SubintervalT *subintervals;
    int chunkSize = 0;
    int totalPrimes = 0;

    pthread_t *threads;
    int       **states;

    if (data.isError)
    {
        exit(-1);
    }

    printf("[Thread MAIN] Parsing ok . . . \n");

    threads = (pthread_t*)malloc(data.numThreads*sizeof(pthread_t));
    
    states = (int**)malloc(data.numThreads*sizeof(int*));
    for(i=0; i<data.numThreads; i++)
    {
        states[i] = (int*)malloc(sizeof(int));
    }

    subintervals = (SubintervalT*)malloc(data.numThreads * sizeof(SubintervalT));
    chunkSize = (data.interEnd-data.interStart) / data.numThreads;


    printf("[Thread MAIN] creating %d threads . . . \n", data.numThreads);
    for(i=0; i<data.numThreads-1; i++)
    {
        subintervals[i].start = i*chunkSize + 1;
        subintervals[i].end = subintervals[i].start + chunkSize -1;

 //       printf("[Thread MAIN] [step:%d] [from:%d to:%d] \n", i, subintervals[i].start, \
                                                        subintervals[i].end);

        if(pthread_create(&threads[i], NULL, countPrimes, &subintervals[i]) != 0)
        {
            printf("ERROR THREAD CREATE!\n");
        }
    }
    
    // Give last chuck to last thread
    subintervals[i].start = subintervals[i-1].end + 1;
    subintervals[i].end   = data.interEnd;
//    printf("[Thread MAIN] [step:%d] [from:%d to:%d] \n", i, subintervals[i].start, \
                                                    subintervals[i].end);
    if(pthread_create(&threads[i], NULL, countPrimes, &subintervals[i]) != 0)
    {
            printf("ERROR THREAD CREATE!\n");
    }


    for(i=0; i<data.numThreads; i++)
    {
        pthread_join(threads[i], (void**)&states[i]);
    }

    for(i=0; i<data.numThreads; i++)
    {
        printf("[Thread MAIN]Ret val=%d\n", *states[i]);
        totalPrimes += *states[i];
    }

    printf("[Thread MAIN]TOTAL PRIMES:%d\n", totalPrimes);

    return 0;
}