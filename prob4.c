/*
Write a server type C program that periodically creates threads that 
simulate   the   handling   of   some   requests   from   clients.   The   server 
threads display a message, wait for a while (using  sleep  function) 
and   then   terminate.  In   the   same   time,  the   server  also   accepts 
commands introduced from keyboard. Implement the stop command 
of the server,  like when  a special key is pressed (for example ‘x’): 
the process should  not create new threads and end. But ending the 
process should be done in a clean state, only after the termination of 
the existing threads at that time.  
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <unistd.h>

#include <pthread.h>

#define MAXTHREADS 100

void *threadFunc(void *arguments)
{
    printf("[hello, A_THREAD:Sleeping 3s]\n");
    sleep(8);
    return 0;
}

int main()
{
    printf("PID=%d\n", getpid());

    int numCreated = 0;
    pthread_t threads[MAXTHREADS];
    int *states[MAXTHREADS];
    int i;



    for(i=0; i<MAXTHREADS; i++)
    {
        pthread_create(&threads[i], NULL, threadFunc, NULL);
        numCreated++;
        printf("[MAIN]Created thread#%d\n", i);
        if(getchar()=='x') break;
    }

   for(i=0; i<=numCreated; i++)
   {
       pthread_join(threads[i], (void**)&states[i]);
   }
    
return 0;
}